import java.util.Random;
import java.util.Scanner;

public class numberGuess {
    public static void main(String[] args) {
        int answer, guess;
        final int MAX = 10;
        int chances = 0, maxAttempts = 3;


        Scanner sc = new Scanner(System.in);
        Random rand = new Random();

        answer = rand.nextInt(MAX) + 1;
        System.out.println("What is your  name?");

        String name = sc.nextLine();

        System.out.println(name + " Welcome to the guess the number game!");



        do
        {


//counts number of guesses
            chances++;


            System.out.println(  " Please guess a number between 1 and 10");
            guess = sc.nextInt();



            if(guess < answer)
            {
                System.out.println("Sorry " + name + " your guess was to low!");
            }

            else if(guess > answer)
            {
                System.out.println(  name + " your guess was to high!");
            }


            else
            {

                System.out.println("Good job " + name + " your guess was correct it took you " + chances + " guesses");
            }

            if(chances <= maxAttempts && guess == answer)
            {
                System.out.println(" You got it! ");

            }
            else if(chances == maxAttempts && guess != answer)

            {
                System.out.println("Close but you ran out of guesses! the answer was " + answer);

            }


        }while(guess != answer  && chances < maxAttempts);


    }
}
